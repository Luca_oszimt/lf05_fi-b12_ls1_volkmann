﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
      
       double zuZahlenderBetrag = fahrkartenbestellungErfassen(); 
       double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);
       
    }
    
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner sc = new Scanner(System.in);
    	double ticketPreis;
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
        ticketPreis = sc.nextDouble();

        // 5.
        System.out.print("Anzahl der Tickets: ");
        int ticketAnzahl = sc.nextInt();
        
        // 6.
        if(ticketAnzahl > 1) {
     	   ticketPreis = ticketPreis * ticketAnzahl;
        }
        
        return ticketPreis;
    }
    
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	// Geldeinwurf
        // -----------
    	Scanner sc = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        double rueckgabeBetrag;
        
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlen - eingezahlterGesamtbetrag), " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = sc.nextDouble();
     	   if(eingeworfeneMünze <= 2) {
     		   eingezahlterGesamtbetrag += eingeworfeneMünze;
     	   } else {
     		   System.out.println("Es können höchstens 2 Euro eingeworfen werden, versuchen Sie es bitte erneut");
     	   }
     	}
        return rueckgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;
    }
    
    
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
    public static void rueckgeldAusgeben(double rueckgabeBetrag) {
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        if(rueckgabeBetrag > 0.0)
        {
     	   System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ", rueckgabeBetrag, " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabeBetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabeBetrag -= 2.0;
            }
            while(rueckgabeBetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabeBetrag -= 1.0;
            }
            while(rueckgabeBetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabeBetrag -= 0.5;
            }
            while(rueckgabeBetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabeBetrag -= 0.2;
            }
            while(rueckgabeBetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabeBetrag -= 0.1;
            }
            while(rueckgabeBetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabeBetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
}


/**
 * 1. 
 * tastatur					- Scanner
 * zuZahlenderBetrag 		- double
 * eingezahlterGesamtbetrag	- double
 * eingeworfeneMünze		- double
 * rückgabebetrag			- double
 * ticketAnzahl				- int
 * i						- int
 */

/**
 * 2.
 * +	Summe
 * ++	Präinkrement
 * -	Differenz
 * *	Produkt
 * =	einfache Zuweisung
 * +=	Additionszuweisung
 * -=	Subtraktionszuweisung
 * >=	Gößer gleich
 * >	Größer
 * <	Kleiner
 */

/**
 * 5.
 * hier wird die Anzahl der Tickets in einer Integer Variable gespeichert.
 * Ich habe den Datentypen Integer (Ganzzahlen) gewählt, da nur ganze Tickets 
 * ausgegeben werden können.
 */

/**
 * 6.
 * Wenn die Ticketsanzahl größer als 1 ist, dann wird der Preis für ein 
 * Ticket mit der Anzahl der Tickets multipliziert, sodass der gesamtbetrag 
 * aller Tickets bezahlt werden muss.
 */