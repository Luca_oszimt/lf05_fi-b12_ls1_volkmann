
public class Aufgabe5_7_Mathe {
	
	public static void main(String[] args) {
		System.out.println(hypotenuse(5, 6));
	}
	//Aufgabe 5
	public static double quadrat(double x) {
		return x*x;
	}
	//Aufgabe 7
	public static double hypotenuse(double kathete1, double kathete2) {
		double h = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
		return h;
	}
}
