import java.util.Scanner;

public class Aufgabe3_4_Widerstaende {
	public static void main(String[] args) {
		double r1 = 5;
		double r2 = 10;
		
		System.out.println("r1=" + r1 + ", r2=" + r2);
		System.out.println("\nErsatzwiderstände:");
		System.out.println("Reihenschaltung = " + reihenschaltung(r1, r2));
		System.out.println("Parallelschaltung = " + parallelschaltung(r1, r2));
	}
	//Aufgabe 3
	public static double reihenschaltung(double r1, double r2) {
		double rges = r1 + r2;
		return rges;
	}
	//Aufgabe 4
	public static double parallelschaltung(double r1, double r2) {
		double rges = 1.0 / (1.0 / r1 + 1.0 / r2);
		return rges;
	}
	
}
