import java.util.Scanner;
public class Aufgabe6_Fahrsimulator {
	public static void main(String[] args) {
		Aufgabe6_Fahrsimulator aF = new Aufgabe6_Fahrsimulator();
		
		double v = 0.0;
		double dv;
		boolean vRange = true;
		
		while(vRange) {
			Scanner sc = new Scanner(System.in);
			dv = sc.nextDouble();
			
			if((v+dv) < 0) {
				v = 0;
				System.out.println("Du kannst nicht R�ckw�rts fahren!");
			} else if((v+dv) > 130) {
				v = 130;
				System.out.println("Die H�chstgeschwindigkeit wurde erreicht");
			} else {
				v = aF.beschleunige(v, dv);
			}
			
			System.out.println("Geschwindigkeit: " + v + " km/h");
		}
	}
	
	public double beschleunige(double v, double dv) {
		v = v + dv;
		return v;
	}
}
