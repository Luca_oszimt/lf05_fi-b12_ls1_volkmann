import java.util.Scanner;

public class Aufgabe2_PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
	
	public static String liesString(String text) {
		Scanner sc = new Scanner(System.in);
		System.out.println(text);
		String input = sc.nextLine();
		return input;
	}
	
	public static int liesInt(String text) {
		Scanner sc = new Scanner(System.in);
		System.out.println(text);
		int input = sc.nextInt();
		return input;
	}
	
	public static double liesDouble(String text) {
		Scanner sc = new Scanner(System.in);
		System.out.println(text);
		double input = sc.nextDouble();
		return input;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double gesamtnettopreis = anzahl * nettopreis;
		return gesamtnettopreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double gesamtbruttopreis = nettogesamtpreis * (1 + mwst / 100);
		return gesamtbruttopreis;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
