
public class Aufgabe2 {

	public static void main(String[] args) {
		// 0! Ergebnisse
		System.out.printf("%-5s= %-19s= %4d\n", "0!", "", 1);
		
		// 1! Ergebnisse
		System.out.printf("%-5s= %-19s= %4d\n", "1!", "1", 1);
		
		// 2! Ergenisse
		System.out.printf("%-5s= %-19s= %4d\n", "2!", "1 * 2", 2);
		
		// 3! Ergebnisse
		System.out.printf("%-5s= %-19s= %4d\n", "3!", "1 * 2 * 3", 6);
		
		// 4! Ergebnisse
		System.out.printf("%-5s= %-19s= %4d\n", "4!", "1 * 2 * 3 * 4", 24);
		
		// 5! Ergebnisse
		System.out.printf("%-5s= %-19s= %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 120);
	}

}
