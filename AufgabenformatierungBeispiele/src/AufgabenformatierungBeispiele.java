
public class AufgabenformatierungBeispiele {
	public static void main(String[] args) {
		// Ganzzahl
		System.out.printf("|%+-10d%+d| \n", 123456, -123456);
		
		// Kommazahl
		System.out.printf("|%+-10.2f| \n", 12.123456789);
		
		// Zeichenketten
		System.out.printf("|%-10.3s| \n", "Max Mustermann");
		
		// �bung
		System.out.printf("Name:%-10sAlter:%-8dGewicht:%10.2f", "Max", 18, 80.50);
	}
}
